{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "patient-fashion",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Making request with params {'motsCles': 'Data Analyste', 'region': 84, 'sort': 1}\n",
      "Token has not been requested yet. Requesting token\n",
      "Now requesting token\n"
     ]
    }
   ],
   "source": [
    "from offres_emploi import Api\n",
    "import pandas as pd\n",
    "\n",
    "\n",
    "def offre_de_emploi():\n",
    "\n",
    "   \n",
    "    client = Api(client_id=\"PAR_reg_01c23e995e7d57bb71b29d988d9126b5064e2663228a0799a692b586b488faab\", \n",
    "             client_secret=\"3adb80b142c29459c2652636a169849fe793e5f1ac0d8ec0e8e95e701419a8c2\")\n",
    "    \n",
    "          \n",
    "    params = {\n",
    "    \"motsCles\": \"Data Analyste\",   \n",
    "    \"region\": 84,\n",
    "    \"sort\" : 1\n",
    "    \n",
    "    }\n",
    "    \n",
    "    pole = { #\"id\":[],\n",
    "            \"info\":[], \n",
    "            \"Link\":[], \n",
    "            #\"dateActualisation\": [],\n",
    "            \"Entreprise\": [],\n",
    "            #\"romeLibelle\":[],\n",
    "            \"time\":[],\n",
    "            #\"typeContrat\":[],\n",
    "            #\"typeContratLibelle\":[],\n",
    "            \"offre\":[],\n",
    "            \"Loc\": []}\n",
    "    basic_search = client.search(params=params)\n",
    "    for i in range(len(basic_search['resultats'])):\n",
    "       # pole[\"id\"].append(basic_search['resultats'][i]['id'])\n",
    "        pole[\"info\"].append(basic_search['resultats'][i]['intitule'])\n",
    "        pole[\"time\"].append(basic_search['resultats'][i]['dateCreation'])\n",
    "       # pole[\"dateActualisation\"].append(basic_search['resultats'][i]['dateActualisation'])\n",
    "        pole[\"Loc\"].append(basic_search['resultats'][i]['lieuTravail']['libelle'])\n",
    "       # pole[\"romeLibelle\"].append(basic_search['resultats'][i]['romeLibelle'])\n",
    "        #pole[\"offre\"].append(basic_search['resultats'][i]['appellationlibelle'])\n",
    "        pole[\"offre\"].append(basic_search['resultats'][i]['typeContrat'])\n",
    "       # pole[\"typeContratLibelle\"].append(basic_search['resultats'][i]['typeContratLibelle'])\n",
    "        pole[\"Link\"].append(basic_search['resultats'][i]['origineOffre'][\"urlOrigine\"])\n",
    "        \n",
    "        if \"nom\" in basic_search['resultats'][i]['entreprise']:\n",
    "            pole[\"Entreprise\"].append(basic_search['resultats'][i]['entreprise'][\"nom\"])\n",
    "        else:\n",
    "            pole[\"Entreprise\"].append(\"voir annonce\")\n",
    "        \n",
    "\n",
    "    df = pd.DataFrame(pole)\n",
    "    \n",
    "    df.to_csv(\"Data_Analyste.csv\", index =False)\n",
    "    \n",
    "    \n",
    "offre_de_emploi()\n",
    "\n",
    "from offres_emploi import Api\n",
    "import pandas as pd\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "valid-pension",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>0</th>\n",
       "      <th>1</th>\n",
       "      <th>2</th>\n",
       "      <th>3</th>\n",
       "      <th>4</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>info</th>\n",
       "      <td>Link</td>\n",
       "      <td>Entreprise</td>\n",
       "      <td>time</td>\n",
       "      <td>offre</td>\n",
       "      <td>Loc</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>Assistant(e) Administratif(ve)/ Data Analyste (H/F)</th>\n",
       "      <td>https://candidat.pole-emploi.fr/offres/recherc...</td>\n",
       "      <td>CASH MANAGEMENT SERVICES</td>\n",
       "      <td>2021-01-18T14:57:25.000Z</td>\n",
       "      <td>CDI</td>\n",
       "      <td>69 - RILLIEUX LA PAPE</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                                                                                                    0  \\\n",
       "info                                                                                             Link   \n",
       "Assistant(e) Administratif(ve)/ Data Analyste (...  https://candidat.pole-emploi.fr/offres/recherc...   \n",
       "\n",
       "                                                                           1  \\\n",
       "info                                                              Entreprise   \n",
       "Assistant(e) Administratif(ve)/ Data Analyste (...  CASH MANAGEMENT SERVICES   \n",
       "\n",
       "                                                                           2  \\\n",
       "info                                                                    time   \n",
       "Assistant(e) Administratif(ve)/ Data Analyste (...  2021-01-18T14:57:25.000Z   \n",
       "\n",
       "                                                        3  \\\n",
       "info                                                offre   \n",
       "Assistant(e) Administratif(ve)/ Data Analyste (...    CDI   \n",
       "\n",
       "                                                                        4  \n",
       "info                                                                  Loc  \n",
       "Assistant(e) Administratif(ve)/ Data Analyste (...  69 - RILLIEUX LA PAPE  "
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df1 = pd.read_csv(\"Data_Analyste.csv\", header=None, names=range(5))\n",
    "df1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "veterinary-crazy",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
