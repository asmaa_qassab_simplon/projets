{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "terminal-submission",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Making request with params {'motsCles': 'data', 'region': 84, 'sort': 1}\n",
      "Token has not been requested yet. Requesting token\n",
      "Now requesting token\n"
     ]
    }
   ],
   "source": [
    "from offres_emploi import Api\n",
    "import pandas as pd\n",
    "\n",
    "\n",
    "def offre_de_emploi():\n",
    "\n",
    "   \n",
    "    client = Api(client_id=\"PAR_reg_01c23e995e7d57bb71b29d988d9126b5064e2663228a0799a692b586b488faab\", \n",
    "             client_secret=\"3adb80b142c29459c2652636a169849fe793e5f1ac0d8ec0e8e95e701419a8c2\")\n",
    "    \n",
    "          \n",
    "    params = {\n",
    "    \"motsCles\": \"data\",\n",
    "    \"region\": 84,\n",
    "    \"sort\" : 1\n",
    "    \n",
    "    }\n",
    "    \n",
    "    pole = {\"id\":[],\n",
    "            \"intitule\":[], \n",
    "            \"dateCreation\":[], \n",
    "            \"dateActualisation\": [],\n",
    "            \"lieuTravail\": [],\n",
    "            \"romeLibelle\":[],\n",
    "            \"appellationlibelle\":[],\n",
    "            \"typeContrat\":[],\n",
    "            \"typeContratLibelle\":[],\n",
    "            \"urlOrigine\":[],\n",
    "            \"entreprise\": []}\n",
    "    \n",
    "    basic_search = client.search(params=params)\n",
    "    \n",
    "    for i in range(len(basic_search['resultats'])):\n",
    "        pole[\"id\"].append(basic_search['resultats'][i]['id'])\n",
    "        pole[\"intitule\"].append(basic_search['resultats'][i]['intitule'])\n",
    "        pole[\"dateCreation\"].append(basic_search['resultats'][i]['dateCreation'])\n",
    "        pole[\"dateActualisation\"].append(basic_search['resultats'][i]['dateActualisation'])\n",
    "        pole[\"lieuTravail\"].append(basic_search['resultats'][i]['lieuTravail']['libelle'])\n",
    "        pole[\"romeLibelle\"].append(basic_search['resultats'][i]['romeLibelle'])\n",
    "        pole[\"appellationlibelle\"].append(basic_search['resultats'][i]['appellationlibelle'])\n",
    "        pole[\"typeContrat\"].append(basic_search['resultats'][i]['typeContrat'])\n",
    "        pole[\"typeContratLibelle\"].append(basic_search['resultats'][i]['typeContratLibelle'])\n",
    "        pole[\"urlOrigine\"].append(basic_search['resultats'][i]['origineOffre'][\"urlOrigine\"])\n",
    "        \n",
    "        if \"nom\" in basic_search['resultats'][i]['entreprise']:\n",
    "            pole[\"entreprise\"].append(basic_search['resultats'][i]['entreprise'][\"nom\"])\n",
    "        else:\n",
    "            pole[\"entreprise\"].append(\"voir annonce\")\n",
    "        \n",
    "\n",
    "    df = pd.DataFrame(pole)\n",
    "    \n",
    "    df.to_csv(\"offre_de_emploi.csv\", index =False)\n",
    "    \n",
    "    \n",
    "offre_de_emploi()\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 50,
   "id": "paperback-bookmark",
   "metadata": {},
   "outputs": [],
   "source": [
    "!curl -X GET \"https://api.emploi-store.fr/partenaire/offresdemploi/v2/offres/search\" -H  \"accept: application/json\" -H  \"Authorization: Bearer MhZhbqecrX4QvpZN2_BRufC8vPg\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "passing-pollution",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "virgin-lover",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
