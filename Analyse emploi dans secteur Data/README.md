    Technologies utilisées :
    Base de données : PostgreSQL
    Langages de programmation : Python, SQL, Bash
    Extraction de données : Scraping, API
    Méthode agile SCRUM
    Déploiement : GitLab Pages
    Lien du projet : Analyse emploi dans secteur Data

Durant ce projet d’une durée de trois semaines et par groupe de quatre nous devions réaliser un tableau de bord sur les métiers de la data. L’objectif de ces 3 semaines était de mettre en valeur la méthodologie SCRUM en attribuant des rôles aux personnes du groupe et effectuant aux mieux les étapes de la méthodologie SCRUM ( scrum review, sprint planning / backlog etc).
De plus, nous devions prendre contact avec notre faux client pour des propositions et des réunions toutes les semaines.
Concernant l’extraction des données, nous avons choisi de ne prendre qu’trois site (pôle emploi,indeed,job) avec son API mais aussi en utilisant surtout la méthode scrapping.
Pour le nettoyage de la données, nous avons utilisé pandas pour les dataframes ainsi que PostgreSQL pour notre base de données. Nos scripts Python ont été structuré sous forme de modules qui sont composées elles-mêmes de fonctions Python. Pour la visualisation, tout a été fait par des modules pythons tels que Folium, Matplotlib, Numpy.
