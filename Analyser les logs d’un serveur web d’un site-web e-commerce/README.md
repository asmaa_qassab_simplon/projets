Informations projet

    Technologies utilisées :
    Base de données : SQLite
    Langages de programmation : Python + regex, SQL, Bash
    Méthode agile SCRUM
    Gitlab
    Lien du projet : Analyser les logs d’un serveur web d’un site-web e-commerce

Durant ce projet d’une durée d’une semaine nous avions récupéré les logs d’un serveur d’un site web de e-commerce. Nous devions effectuer une mesure d’audience des visiteurs et plus précisément répondre à différentes questions demandé par un client fictif. Pour ce faire, j’ai réalisé un pipeline BASH permettant de collecter la donnée, la traiter et répondre aux questions. Le script Bash fait appel à un script python qui permet de récupérer Les logs du serveur web au format “Common Log Format” et de les intégrer dans un fichier CSV pour une future analyse. Un autre fichier permet d’importer les données traitées dans une base de données SQL et y pouvoir faire des requêtes.
