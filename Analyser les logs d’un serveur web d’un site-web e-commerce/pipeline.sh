#!/bin/

# Ce script automatise un pipeline de données.
#
# Utilisation:
#   ./pipeline.sh

DATA_URL="https://dataverse.harvard.edu/api/access/datafile/:persistentId?persistentId=doi:10.7910/DVN/3QBYB5/NXKB6J" 
DATA_FILE=access.log
DATABASE_FILE=access_db.db
##############################################################################
 wget $DATA_URL -qO $DATA_FILE



unzip ${DATA_FILE}
rm -r __MACOSX


##############################################################################
echo "Téléchargement du dataset ${DATA_FILE} (${DATA_URL})"
##############################################################################
# Partie 2 : chargement de données
##############################################################################

python3 python2.py

sqlite3 $DATABASE_FILE  ".read load.sql" 


echo "Chargement des données en base ${DATABASE_FILE} (load.sql)"

sudo cp ${DATABASE_FILE} .docker/metabase/

echo "we put  ${DATABASE_FILE} in Metabase"

#rm ${DATA_FILE}

#!bash 



#########################################
python3 python3.py

sqlite3 $DATABASE_FILE  ".read transform.sql" 




sudo cp ${DATABASE_FILE} .docker/metabase/

echo "we put  ${DATABASE_FILE} in Metabase"

#########################################

python3 python4.py

sqlite3 $DATABASE_FILE  ".read transform1.sql" 




sudo cp ${DATABASE_FILE} .docker/metabase/

echo "we put  ${DATABASE_FILE} in Metabase"






