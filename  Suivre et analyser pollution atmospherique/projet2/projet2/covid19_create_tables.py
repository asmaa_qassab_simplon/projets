import pandas        # librairie qui permet de manipuler des données - Dataframes
import requests      # Librairie HTTP
import json          # Librairie JSON pour formater les chaînes JSON
import psycopg2      # Librairie Connect to postgres DB


from pandas import DataFrame
from psycopg2.extensions import parse_dsn


db_dsn = "postgres://postgres:test@localhost:5432/covid19"
db_args = parse_dsn(db_dsn)
conn = psycopg2.connect(**db_args)
cur = conn.cursor()
cur.execute("""CREATE TABLE IF NOT EXISTS pollution( 
                   "longitude" Real,
                   "lattitude" Real,
                   "nom_dept" text,
                   "nom_com" text,
                   "insee_com" text,
                   "nom_station" text,
                   "code_station" text,
                   "typologie" text,
                   "influence" text,
                   "nom_poll" text,
                   "id_poll_ue" integer ,
                   "valeur" real,
                   "unite" text,
                   "metrique" text ,
                   "date_debut" timestamp with time zone,
                   "date_fin" timestamp with time zone,
                   "x_wgs84" Real ,
                   "y_wgs84" Real,
                   "x_reglementaire" real ,
                   "y_reglementaire" Real ,
                   "OBJECTID" INT);""")
    
cur.execute("DELETE FROM pollution")
cur.execute("COPY pollution FROM '/data/covid.csv' (FORMAT 'csv', DELIMITER ',', HEADER TRUE);")
    
cur.execute("""CREATE TABLE IF NOT EXISTS dept(
        "id" SERIAL,
        "nom_dept" TEXT,
        PRIMARY KEY ("id"));""")

cur.execute("""CREATE TABLE IF NOT EXISTS poll(
        "id" SERIAL,
        "nom_poll" TEXT,
        PRIMARY KEY ("id"));""")
    
    
cur.execute("""CREATE TABLE IF NOT EXISTS com (
        "id" SERIAL,
        "nom_com" TEXT,
        
        PRIMARY KEY ("id"));""")
        

cur.execute("""CREATE TABLE IF NOT EXISTS station(
        "id" SERIAL,
        "nom_station" TEXT,
        "influence" TEXT,
        "typologie" TEXT,
        "longitude" FLOAT,
        "lattitude" FLOAT,
        PRIMARY KEY ("id"));""")

    
cur.execute("""CREATE TABLE IF NOT EXISTS polldata (
        "com_id" INTEGER,
        "dept_id" INTEGER,
        "station_id" INTEGER,
        "poll_id" INTEGER, 
        "valeur" NUMERIC NULL,
        "date_debut" TIMESTAMP WITH TIME ZONE,
        "date_fin" TIMESTAMP WITH TIME ZONE,
        FOREIGN KEY ("com_id") REFERENCES com ("id"),
        FOREIGN KEY ("dept_id") REFERENCES dept ("id"),
        FOREIGN KEY ("station_id") REFERENCES station ("id"),
        FOREIGN KEY ("poll_id") REFERENCES poll ("id"))
        ;""")


conn.commit()

cur.close()
conn.close() 
    

db_dsn = "postgres://postgres:test@localhost:5432/covid19"
db_args = parse_dsn(db_dsn)
conn = psycopg2.connect(**db_args)

cur = conn.cursor()
cur.execute("DELETE FROM polldata;")
cur.execute("DELETE FROM dept;")
cur.execute("DELETE FROM poll;")
cur.execute("DELETE FROM com;")
cur.execute("DELETE FROM station;")

    
cur.execute("""INSERT INTO dept ("nom_dept")
    SELECT DISTINCT(pollution.nom_dept)
    FROM pollution""")
    
       
cur.execute("""INSERT INTO poll ("nom_poll")
    SELECT DISTINCT(pollution.nom_poll)
    FROM pollution""")
    
            
cur.execute("""INSERT INTO com ("nom_com") 
    SELECT DISTINCT (pollution.nom_com) 
    FROM pollution ; """)
    
cur.execute("""INSERT INTO station("nom_station", "influence","typologie", "longitude", "lattitude") 
    SELECT DISTINCT (pollution.nom_station),pollution.influence,pollution.typologie, pollution.longitude, pollution.lattitude  
    FROM pollution; """)
         
cur.execute("""INSERT INTO polldata ("date_debut","date_fin","valeur","com_id","dept_id" ,"station_id","poll_id") 
    SELECT pollution.date_debut ,pollution.date_fin,pollution.valeur ,com.id, dept.id ,station.id,poll.id 
    FROM pollution
    JOIN com
    ON com.nom_com = pollution.nom_com
    JOIN dept
    ON dept.nom_dept = pollution.nom_dept
    JOIN station
    ON  station.nom_station = pollution.nom_station
    JOIN poll
    ON poll.nom_poll = pollution.nom_poll
    
    ;""")
#cur.execute("""SELECT * from polldata LIMIT 10;""")
#test = cur.fetchall()
#print(test)
    
    
conn.commit()
cur.close()
conn.close()