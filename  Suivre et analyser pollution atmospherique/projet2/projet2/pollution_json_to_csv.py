import pandas 
import requests 
import json
from pandas import DataFrame

DATA_FILE="pollution.csv"

pm10 = requests.get('https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_4.geojson')

no2 = requests.get('https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_1.geojson')

data_pm10 = pm10.json()
data_no2 = no2.json()

pollution = {"longitude":[],"lattitude":[], "nom_dept":[], "nom_com":[], "insee_com":[], "nom_station":[], "code_station":[], "typologie":[], "influence":[], "nom_poll":[],  "id_poll_ue":[], "valeur":[], "unite":[], "metrique":[], "date_debut":[], "date_fin":[], "x_wgs84":[], "y_wgs84":[], "x_reglementaire":[], "y_reglementaire":[],"OBJECTID":[]}

for i in range(len(data_pm10["features"])):

    pollution["longitude"].append(data_pm10["features"][i]["geometry"]["coordinates"][0])
    pollution["lattitude"].append(data_pm10["features"][i]["geometry"]["coordinates"][1])
    pollution["nom_dept"].append(data_pm10["features"][i]["properties"]["nom_dept"])
    pollution["nom_com"].append(data_pm10["features"][i]["properties"]["nom_com"])
    pollution["insee_com"].append(data_pm10["features"][i]["properties"]["insee_com"])
    pollution["nom_station"].append(data_pm10["features"][i]["properties"]["nom_station"])
    pollution["code_station"].append(data_pm10["features"][i]["properties"]["code_station"])
    pollution["typologie"].append(data_pm10["features"][i]["properties"]["typologie"])
    pollution["influence"].append(data_pm10["features"][i]["properties"]["influence"])
    pollution["nom_poll"].append(data_pm10["features"][i]["properties"]["nom_poll"])
    pollution["id_poll_ue"].append(data_pm10["features"][i]["properties"]["id_poll_ue"])
    pollution["valeur"].append(data_pm10["features"][i]["properties"]["valeur"])
    pollution["unite"].append(data_pm10["features"][i]["properties"]["unite"])
    pollution["metrique"].append(data_pm10["features"][i]["properties"]["metrique"])
    pollution["date_debut"].append(data_pm10["features"][i]["properties"]["date_debut"])
    pollution["date_fin"].append(data_pm10["features"][i]["properties"]["date_fin"])
    pollution["x_wgs84"].append(data_pm10["features"][i]["properties"]["x_wgs84"])
    pollution["y_wgs84"].append(data_pm10["features"][i]["properties"]["y_wgs84"])
    pollution["x_reglementaire"].append(data_pm10["features"][i]["properties"]["x_reglementaire"])
    pollution["y_reglementaire"].append(data_pm10["features"][i]["properties"]["y_reglementaire"])
    pollution["OBJECTID"].append(data_pm10["features"][i]["properties"]["OBJECTID"])
    
for i in range(len(data_no2["features"])):
    
    pollution["longitude"].append(data_no2["features"][i]["geometry"]["coordinates"][0])
    pollution["lattitude"].append(data_no2["features"][i]["geometry"]["coordinates"][1])
    pollution["nom_dept"].append(data_no2["features"][i]["properties"]["nom_dept"])
    pollution["nom_com"].append(data_no2["features"][i]["properties"]["nom_com"])
    pollution["insee_com"].append(data_no2["features"][i]["properties"]["insee_com"])
    pollution["nom_station"].append(data_no2["features"][i]["properties"]["nom_station"])
    pollution["code_station"].append(data_no2["features"][i]["properties"]["code_station"])
    pollution["typologie"].append(data_no2["features"][i]["properties"]["typologie"])
    pollution["influence"].append(data_no2["features"][i]["properties"]["influence"])
    pollution["nom_poll"].append(data_no2["features"][i]["properties"]["nom_poll"])
    pollution["id_poll_ue"].append(data_no2["features"][i]["properties"]["id_poll_ue"])
    pollution["valeur"].append(data_no2["features"][i]["properties"]["valeur"])
    pollution["unite"].append(data_no2["features"][i]["properties"]["unite"])
    pollution["metrique"].append(data_no2["features"][i]["properties"]["metrique"])
    pollution["date_debut"].append(data_no2["features"][i]["properties"]["date_debut"])
    pollution["date_fin"].append(data_no2["features"][i]["properties"]["date_fin"])
    pollution["x_wgs84"].append(data_no2["features"][i]["properties"]["x_wgs84"])
    pollution["y_wgs84"].append(data_no2["features"][i]["properties"]["y_wgs84"])
    pollution["x_reglementaire"].append(data_no2["features"][i]["properties"]["x_reglementaire"])
    pollution["y_reglementaire"].append(data_no2["features"][i]["properties"]["y_reglementaire"])
    pollution["OBJECTID"].append(data_no2["features"][i]["properties"]["OBJECTID"])
    

data_f = DataFrame(pollution)
data_f.to_csv(DATA_FILE, index=None)


data_f = DataFrame(pollution)
#print (data_f.iloc[1:3])

