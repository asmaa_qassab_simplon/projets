clear
echo -e "\033[31m "ce script va effectuer les opérations suivantes:" \033[0m"
echo -e "\033[31m "1 - Down - Up des process Docker Postgres - Metabase" \033[0m"
echo -e "\033[31m "2 - Création et alimentation des Databases Projet 2" \033[0m"
echo "Voulez-vous continuer ? [O/N]"
read mot
if [ "$mot" = "O" ]
then
echo -e "\033[32m ""Script en cours d'execution"" \033[0m"

sudo docker-compose down
sudo docker-compose up -d
sudo docker-compose ps

echo "Création covid db"
echo "Création pollution db"
python3 create_base.py

echo "Création covid.csv"
echo "Création pollution.csv"
python3 covid19_json_to_csv.py
python3 pollution_json_to_csv.py

echo "Copie des 2 csv dans Postgres"
sudo cp covid.csv .docker/data/
sudo cp pollution.csv .docker/data/

echo "Création tables - import Data dans les 2 db"
python3 covid19_create_tables.py
python3 pollution_create_tables.py

echo -e "\033[32m"Script terminé" \033[0m"

else
echo -e "\033[31m "script interrompu" \033[0m"
fi
