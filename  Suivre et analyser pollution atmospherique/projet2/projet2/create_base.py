import psycopg2      # Librairie Connect to postgres DB
from psycopg2.extensions import parse_dsn
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
#from psycopg2 import connect, extensions, sql

# string for the new database name to be created
sql = '''CREATE database covid19'''
sql1 = '''CREATE database pollution'''


db_dsn = "postgres://postgres:test@localhost:5432/"
db_args = parse_dsn(db_dsn)
#con = psycopg2.connect("user='postgres' password='test'")
conn = psycopg2.connect(**db_args)
cur = conn.cursor()
conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
cur.execute(sql)
cur.execute(sql1)

conn.commit()

cur.close()
conn.close() 