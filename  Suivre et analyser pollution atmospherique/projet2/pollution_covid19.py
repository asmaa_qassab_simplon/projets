{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "   longitude  lattitude     nom_dept           nom_com insee_com  \\\n",
      "0   3.113833  45.797279  Puy-de-Dôme  Clermont-Ferrand     63113   \n",
      "1   3.087500  45.772165  Puy-de-Dôme  Clermont-Ferrand     63113   \n",
      "\n",
      "    nom_station code_station typologie influence          nom_poll  ...  \\\n",
      "0   Montferrand      FR07004   urbaine      fond  monoxyde d'azote  ...   \n",
      "1  Jardin Lecoq      FR07009   urbaine      fond  monoxyde d'azote  ...   \n",
      "\n",
      "  valeur  unite metrique              date_debut                date_fin  \\\n",
      "0    NaN  µg/m3  horaire  2021/01/22 13:00:00+00  2021/01/22 14:00:00+00   \n",
      "1    NaN  µg/m3  horaire  2021/01/22 13:00:00+00  2021/01/22 14:00:00+00   \n",
      "\n",
      "    x_wgs84    y_wgs84  x_reglementaire  y_reglementaire  OBJECTID  \n",
      "0  3.113833  45.797279      708842.3125        6521968.0         1  \n",
      "1  3.087500  45.772165      706799.9375        6519176.5         2  \n",
      "\n",
      "[2 rows x 21 columns]\n"
     ]
    }
   ],
   "source": [
    "import pandas \n",
    "import requests \n",
    "import json\n",
    "\n",
    "from pandas import DataFrame\n",
    "covid19=requests.get('https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_0.geojson')\n",
    "\n",
    "data_covid19 = covid19.json()\n",
    "\n",
    "pollution_covid19 = {\"longitude\":[],\"lattitude\":[], \"nom_dept\":[], \"nom_com\":[], \"insee_com\":[], \"nom_station\":[], \"code_station\":[], \"typologie\":[], \"influence\":[], \"nom_poll\":[],  \"id_poll_ue\":[], \"valeur\":[], \"unite\":[], \"metrique\":[], \"date_debut\":[], \"date_fin\":[], \"x_wgs84\":[], \"y_wgs84\":[], \"x_reglementaire\":[], \"y_reglementaire\":[],\"OBJECTID\":[]}\n",
    "for i in range(len(data_covid19[\"features\"])):\n",
    "\n",
    "    pollution_covid19[\"longitude\"].append(data_covid19[\"features\"][i][\"geometry\"][\"coordinates\"][0])\n",
    "    pollution_covid19[\"lattitude\"].append(data_covid19[\"features\"][i][\"geometry\"][\"coordinates\"][1])\n",
    "    pollution_covid19[\"nom_dept\"].append(data_covid19[\"features\"][i][\"properties\"][\"nom_dept\"])\n",
    "    pollution_covid19[\"nom_com\"].append(data_covid19[\"features\"][i][\"properties\"][\"nom_com\"])\n",
    "    pollution_covid19[\"insee_com\"].append(data_covid19[\"features\"][i][\"properties\"][\"insee_com\"])\n",
    "    pollution_covid19[\"nom_station\"].append(data_covid19[\"features\"][i][\"properties\"][\"nom_station\"])\n",
    "    pollution_covid19[\"code_station\"].append(data_covid19[\"features\"][i][\"properties\"][\"code_station\"])\n",
    "    pollution_covid19[\"typologie\"].append(data_covid19[\"features\"][i][\"properties\"][\"typologie\"])\n",
    "    pollution_covid19[\"influence\"].append(data_covid19[\"features\"][i][\"properties\"][\"influence\"])\n",
    "    pollution_covid19[\"nom_poll\"].append(data_covid19[\"features\"][i][\"properties\"][\"nom_poll\"])\n",
    "    pollution_covid19[\"id_poll_ue\"].append(data_covid19[\"features\"][i][\"properties\"][\"id_poll_ue\"])\n",
    "    pollution_covid19[\"valeur\"].append(data_covid19[\"features\"][i][\"properties\"][\"valeur\"])\n",
    "    pollution_covid19[\"unite\"].append(data_covid19[\"features\"][i][\"properties\"][\"unite\"])\n",
    "    pollution_covid19[\"metrique\"].append(data_covid19[\"features\"][i][\"properties\"][\"metrique\"])\n",
    "    pollution_covid19[\"date_debut\"].append(data_covid19[\"features\"][i][\"properties\"][\"date_debut\"])\n",
    "    pollution_covid19[\"date_fin\"].append(data_covid19[\"features\"][i][\"properties\"][\"date_fin\"])\n",
    "    pollution_covid19[\"x_wgs84\"].append(data_covid19[\"features\"][i][\"properties\"][\"x_wgs84\"])\n",
    "    pollution_covid19[\"y_wgs84\"].append(data_covid19[\"features\"][i][\"properties\"][\"y_wgs84\"])\n",
    "    pollution_covid19[\"x_reglementaire\"].append(data_covid19[\"features\"][i][\"properties\"][\"x_reglementaire\"])\n",
    "    pollution_covid19[\"y_reglementaire\"].append(data_covid19[\"features\"][i][\"properties\"][\"y_reglementaire\"])\n",
    "    pollution_covid19[\"OBJECTID\"].append(data_covid19[\"features\"][i][\"properties\"][\"OBJECTID\"])\n",
    "    \n",
    "\n",
    "data_f = DataFrame(pollution_covid19)\n",
    "data_f.to_csv(\"pollution_covid19.csv\", index=None)\n",
    "\n",
    "\n",
    "data_f = DataFrame(pollution_covid19)\n",
    "print (data_f.iloc[0:2])"
   ]
  },
  {
   "cell_type": "code",

   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
