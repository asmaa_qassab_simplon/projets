![PostgreSQL Logo](https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/1200px-Postgresql_elephant.svg.png)

# Activité Découverte PostgreSQL

> Utilisation d'un système de gestion de base de données relationnelle

## Objectifs

- Découvrir un serveur de base de données moderne



- Utiliser le langage SQL pour communiquer avec un serveur de base de données

## Modalités

Cette activité est **à réaliser individuellement**, mais **en réfléchissant en groupe** :

- Chaque apprenant doit compléter ce document sur sa propre branche nommée `prenom-nom`
- Penser à cocher les cases à chaque étape réalisée
- Compléter chaque emplacement marqué `[A COMPLETER]`

## Partie 1 : recherche d'information

- Qu'est-ce qu'un système de gestion de base de données relationnelle (_SGBDR_ ou _RDBMS_ en anglais) ?

`[ A relational database management system (RDBMS or just RDB) is a common type of database that stores data in tables, so it can be used in relation to other stored datasets. Most databases used by businesses these days are relational databases, as opposed to a flat file or hierarchical database.
]`

- Quels sont les autres _SGBDR_ existants ?

`[ Historically, the most popular of these have been Microsoft SQL Server, Oracle Database, MySQL, and IBM DB2  ]`

- Quelles sont les différences/avantages/inconvénients entre SQLite et PostgreSQL ?

`[
    
SQLITE

SQLite is a software library that provides relational database management system (RDBMS). It was designed by D. Richard Hipp on August 2000. The design goals of SQLite were to allow the program to be operated without installing a database management system (DBMS) or requiring a database administrator.
Some of the features of MySQL are –

    High-reliability
    Embedded
    Transactions follow ACID properties

- Developed by D. Richard Hipp on August 2000.
- It is widely used in-process RDBMS.
- Implementation language is C.
- It has no Secondary database models
- It does not supports XML format.
- SQLite does not require a server to run. Hence, it is serverless.
- It does not support Server-side scripting
- It does not support any replication methods.
- It does not support any Partitioning methods.
- It supports in-memory capabilities.
- SQLite provides ACID transactions.




POSTGRESQL

It a powerful, open-source Object-relational database system. It provides good performance with low maintenance efforts because of its high stability. PostgreSQL was the first DBMS that implemented multi-version concurrency control (MVCC) feature.
Some of the highlights of PostgreSQL are –

    Support for vast amount of languages
    It process advanced Security features
    It has geo-tagging support

- Developed By PostgreSQL Global Development Group on 1989.
- It is widely used open source RDBMS.
- Implementation language is C.
- It has Document store as Secondary database models
- It supports XML format.
- Server operating systems for PostgreSQL are FreeBSD, Linux, OS X, Solaris and Windows.
- It has user defined functions for Server-side scripts
- It support only one replication methods Master-master replication.
- In PostgreSQL, partitioning can be done by range, list and hash.
- It does not supports in-memory capabilities.
- PostgreSQL also provides ACID transactions.








]`

## Partie 2 : installation

Nous allons utiliser Docker pour démarrer un serveur PostgreSQL :

- [ ] Lancer la configuration [`docker-compose.yml`](docker-compose.yml) déjà prête à l'emploi :

```bash
sudo docker-compose up -d
```

- [ ] Vérifier que le service `postgres` sont lancés sans erreur :

```bash
sudo docker-compose ps
```

- [ ] Se connecter au serveur PostgreSQL :

```bash
sudo docker-compose exec postgres psql -h postgres -U postgres decouverte
Password for user postgres: test

psql (13.1 (Debian 13.1-1.pgdg100+1))
Type "help" for help.

decouverte=#
```

## Partie 3 : types de données

Une des forces d'un _SGBDR_ comme PostgreSQL
S.NO.
SQLITE
POSTGRESQL
1.
Developed by D. Richard Hipp on August 2000.
Developed By PostgreSQL Global Development Group on 1989.
2.
It is widely used in-process RDBMS.
It is widely used open source RDBMS.
3.
Implementation language is C.
Implementation language is C.
4.
It has no Secondary database models
It has Document store as Secondary database models
5.
It does not supports XML format.
It supports XML format.
6.
SQLite does not require a server to run. Hence, it is serverless.
Server operating systems for PostgreSQL are FreeBSD, Linux, OS X, Solaris and Windows.
7.
It does not support Server-side scripting
It has user defined functions for Server-side scripts
8.
It does not support any replication methods.
It support only one replication methods Master-master replication.
9.
It does not support any Partitioning methods.
In PostgreSQL, partitioning can be done by range, list and hash.
10.
It supports in-memory capabilities.
It does not supports in-memory capabilities.
11.
SQLite provides ACID transactions.
PostgreSQL also provides ACID transactions.
 réside dans les types de données offerts nativement.

Vous pouvez consulter la [documentation officielle](https://www.postgresql.org/docs/current/datatype.html)
pour prendre connaissance de tous les types de données disponibles dans Postgres.

- [ ] Créer une table `traffic` :

```sql
CREATE TABLE traffic (
    "ip_address" INET,
    "timestamp" TIMESTAMP WITH TIME ZONE,
    "method" VARCHAR(8),
    "resource" VARCHAR(256),
    "protocol" VARCHAR(16),
    "status_code" INTEGER,
    "response_size" INTEGER,
    "referer" TEXT,
    "browser_family" VARCHAR(32),
    "os_family" VARCHAR(32),
    "device_brand" VARCHAR(32),
    "is_mobile" BOOLEAN,
    "is_bot" BOOLEAN
);
```

- [ ] Vérifier que la table est bien créée avec les bons types de colonnes :

```sql
-- lister toutes les tables
\d

-- afficher les détails des tables
\d+

-- afficher les détails de la table `traffic`
\d+ traffic
```

- [ ] Insérer les données du fichier [traffic.csv](traffic.csv) dans la table `traffic` (vous pouvez ajouter des données supplémentaire au fichier CSV si nécessaire) :

```bash
cp traffic.csv .docker/data/
```

```sql
COPY traffic
FROM '/data/traffic.csv' (FORMAT 'csv', DELIMITER ',', HEADER TRUE);
```

- [ ] Vérifier les données insérées dans la table `traffic` :

```sql
SELECT * FROM traffic;
```

- [ ] Filtrer les lignes correspondant uniquement à du traffic mobile :

```sql
SELECT *
FROM traffic
WHERE is_mobile;
```

- [ ] Filtrer les lignes correspondant uniquement à du traffic de bot :

```sql
SELECT *
FROM traffic
WHERE is_bot;
```

- [ ] Filtrer les lignes correspondant uniquement à du traffic mobile sur Android non-bot :

```sql
SELECT *
FROM traffic
WHERE is_mobile AND NOT is_bot AND os_family = 'Android';
```

- [ ] Nombre de requêtes groupées par seconde :

```sql
SELECT timestamp, COUNT(*) as count
FROM traffic
GROUP BY timestamp;
```

- [ ] Filtrer les lignes correspondant uniquement à du traffic depuis l'intervalle d'adresses IP "31.56.96/24" :

```sql
SELECT *
FROM traffic
WHERE ip_address << inet '31.56.96/24';
```

- [ ] Tenter de reproduire d'autres requêtes du Projet 1 ("Mesure d'audience sur le web")

### Observations

`[A COMPLETER]`
