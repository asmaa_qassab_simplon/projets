import pandas 
import requests 
import json
from pandas import DataFrame


pm10 = requests.get('https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_4.geojson')
no2 = requests.get('https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_1.geojson')

data_pm10 = pm10.json()
data_no2 = no2.json()

pm_no = {"longitude":[],"lattitude":[], "nom_dept":[], "nom_com":[], "insee_com":[], "nom_station":[], "code_station":[], "typologie":[], "influence":[], "nom_poll":[],  "id_poll_ue":[], "valeur":[], "unite":[], "metrique":[], "date_debut":[], "date_fin":[], "x_wgs84":[], "y_wgs84":[], "x_reglementaire":[], "y_reglementaire":[],"OBJECTID":[]}

for i in range(len(data_pm10["features"])):

    pm_no["longitude"].append(data_pm10["features"][i]["geometry"]["coordinates"][0])
    pm_no["lattitude"].append(data_pm10["features"][i]["geometry"]["coordinates"][1])
    pm_no["nom_dept"].append(data_pm10["features"][i]["properties"]["nom_dept"])
    pm_no["nom_com"].append(data_pm10["features"][i]["properties"]["nom_com"])
    pm_no["insee_com"].append(data_pm10["features"][i]["properties"]["insee_com"])
    pm_no["nom_station"].append(data_pm10["features"][i]["properties"]["nom_station"])
    pm_no["code_station"].append(data_pm10["features"][i]["properties"]["code_station"])
    pm_no["typologie"].append(data_pm10["features"][i]["properties"]["typologie"])
    pm_no["influence"].append(data_pm10["features"][i]["properties"]["influence"])
    pm_no["nom_poll"].append(data_pm10["features"][i]["properties"]["nom_poll"])
    pm_no["id_poll_ue"].append(data_pm10["features"][i]["properties"]["id_poll_ue"])
    pm_no["valeur"].append(data_pm10["features"][i]["properties"]["valeur"])
    pm_no["unite"].append(data_pm10["features"][i]["properties"]["unite"])
    pm_no["metrique"].append(data_pm10["features"][i]["properties"]["metrique"])
    pm_no["date_debut"].append(data_pm10["features"][i]["properties"]["date_debut"])
    pm_no["date_fin"].append(data_pm10["features"][i]["properties"]["date_fin"])
    pm_no["x_wgs84"].append(data_pm10["features"][i]["properties"]["x_wgs84"])
    pm_no["y_wgs84"].append(data_pm10["features"][i]["properties"]["y_wgs84"])
    pm_no["x_reglementaire"].append(data_pm10["features"][i]["properties"]["x_reglementaire"])
    pm_no["y_reglementaire"].append(data_pm10["features"][i]["properties"]["y_reglementaire"])
    pm_no["OBJECTID"].append(data_pm10["features"][i]["properties"]["OBJECTID"])
    
for i in range(len(data_no2["features"])):
    
    pm_no["longitude"].append(data_no2["features"][i]["geometry"]["coordinates"][0])
    pm_no["lattitude"].append(data_no2["features"][i]["geometry"]["coordinates"][1])
    pm_no["nom_dept"].append(data_no2["features"][i]["properties"]["nom_dept"])
    pm_no["nom_com"].append(data_no2["features"][i]["properties"]["nom_com"])
    pm_no["insee_com"].append(data_no2["features"][i]["properties"]["insee_com"])
    pm_no["nom_station"].append(data_no2["features"][i]["properties"]["nom_station"])
    pm_no["code_station"].append(data_no2["features"][i]["properties"]["code_station"])
    pm_no["typologie"].append(data_no2["features"][i]["properties"]["typologie"])
    pm_no["influence"].append(data_no2["features"][i]["properties"]["influence"])
    pm_no["nom_poll"].append(data_no2["features"][i]["properties"]["nom_poll"])
    pm_no["id_poll_ue"].append(data_no2["features"][i]["properties"]["id_poll_ue"])
    pm_no["valeur"].append(data_no2["features"][i]["properties"]["valeur"])
    pm_no["unite"].append(data_no2["features"][i]["properties"]["unite"])
    pm_no["metrique"].append(data_no2["features"][i]["properties"]["metrique"])
    pm_no["date_debut"].append(data_no2["features"][i]["properties"]["date_debut"])
    pm_no["date_fin"].append(data_no2["features"][i]["properties"]["date_fin"])
    pm_no["x_wgs84"].append(data_no2["features"][i]["properties"]["x_wgs84"])
    pm_no["y_wgs84"].append(data_no2["features"][i]["properties"]["y_wgs84"])
    pm_no["x_reglementaire"].append(data_no2["features"][i]["properties"]["x_reglementaire"])
    pm_no["y_reglementaire"].append(data_no2["features"][i]["properties"]["y_reglementaire"])
    pm_no["OBJECTID"].append(data_no2["features"][i]["properties"]["OBJECTID"])
    

data_f = DataFrame(pm_no)
data_f.to_csv("pollution.csv", index=None)

