{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "   longitude  lattitude     nom_dept           nom_com insee_com  \\\n",
      "0   3.113833  45.797279  Puy-de-Dôme  Clermont-Ferrand     63113   \n",
      "1   3.087500  45.772165  Puy-de-Dôme  Clermont-Ferrand     63113   \n",
      "\n",
      "    nom_station code_station typologie influence         nom_poll  ... valeur  \\\n",
      "0   Montferrand      FR07004   urbaine      fond  particules PM10  ...    NaN   \n",
      "1  Jardin Lecoq      FR07009   urbaine      fond  particules PM10  ...    NaN   \n",
      "\n",
      "   unite metrique              date_debut                date_fin   x_wgs84  \\\n",
      "0  µg/m3  horaire  2021/01/22 13:00:00+00  2021/01/22 14:00:00+00  3.113833   \n",
      "1  µg/m3  horaire  2021/01/22 13:00:00+00  2021/01/22 14:00:00+00  3.087500   \n",
      "\n",
      "     y_wgs84  x_reglementaire  y_reglementaire  OBJECTID  \n",
      "0  45.797279      708842.3125        6521968.0         1  \n",
      "1  45.772165      706799.9375        6519176.5         2  \n",
      "\n",
      "[2 rows x 21 columns]\n"
     ]
    }
   ],
   "source": [
    "import pandas \n",
    "import requests \n",
    "import json\n",
    "from pandas import DataFrame\n",
    "\n",
    "DATA_FILE=\"pollution.csv\"\n",
    "\n",
    "pm10 = requests.get('https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_4.geojson')\n",
    "\n",
    "no2 = requests.get('https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_1.geojson')\n",
    "\n",
    "data_pm10 = pm10.json()\n",
    "data_no2 = no2.json()\n",
    "\n",
    "pollution = {\"longitude\":[],\"lattitude\":[], \"nom_dept\":[], \"nom_com\":[], \"insee_com\":[], \"nom_station\":[], \"code_station\":[], \"typologie\":[], \"influence\":[], \"nom_poll\":[],  \"id_poll_ue\":[], \"valeur\":[], \"unite\":[], \"metrique\":[], \"date_debut\":[], \"date_fin\":[], \"x_wgs84\":[], \"y_wgs84\":[], \"x_reglementaire\":[], \"y_reglementaire\":[],\"OBJECTID\":[]}\n",
    "\n",
    "for i in range(len(data_pm10[\"features\"])):\n",
    "\n",
    "    pollution[\"longitude\"].append(data_pm10[\"features\"][i][\"geometry\"][\"coordinates\"][0])\n",
    "    pollution[\"lattitude\"].append(data_pm10[\"features\"][i][\"geometry\"][\"coordinates\"][1])\n",
    "    pollution[\"nom_dept\"].append(data_pm10[\"features\"][i][\"properties\"][\"nom_dept\"])\n",
    "    pollution[\"nom_com\"].append(data_pm10[\"features\"][i][\"properties\"][\"nom_com\"])\n",
    "    pollution[\"insee_com\"].append(data_pm10[\"features\"][i][\"properties\"][\"insee_com\"])\n",
    "    pollution[\"nom_station\"].append(data_pm10[\"features\"][i][\"properties\"][\"nom_station\"])\n",
    "    pollution[\"code_station\"].append(data_pm10[\"features\"][i][\"properties\"][\"code_station\"])\n",
    "    pollution[\"typologie\"].append(data_pm10[\"features\"][i][\"properties\"][\"typologie\"])\n",
    "    pollution[\"influence\"].append(data_pm10[\"features\"][i][\"properties\"][\"influence\"])\n",
    "    pollution[\"nom_poll\"].append(data_pm10[\"features\"][i][\"properties\"][\"nom_poll\"])\n",
    "    pollution[\"id_poll_ue\"].append(data_pm10[\"features\"][i][\"properties\"][\"id_poll_ue\"])\n",
    "    pollution[\"valeur\"].append(data_pm10[\"features\"][i][\"properties\"][\"valeur\"])\n",
    "    pollution[\"unite\"].append(data_pm10[\"features\"][i][\"properties\"][\"unite\"])\n",
    "    pollution[\"metrique\"].append(data_pm10[\"features\"][i][\"properties\"][\"metrique\"])\n",
    "    pollution[\"date_debut\"].append(data_pm10[\"features\"][i][\"properties\"][\"date_debut\"])\n",
    "    pollution[\"date_fin\"].append(data_pm10[\"features\"][i][\"properties\"][\"date_fin\"])\n",
    "    pollution[\"x_wgs84\"].append(data_pm10[\"features\"][i][\"properties\"][\"x_wgs84\"])\n",
    "    pollution[\"y_wgs84\"].append(data_pm10[\"features\"][i][\"properties\"][\"y_wgs84\"])\n",
    "    pollution[\"x_reglementaire\"].append(data_pm10[\"features\"][i][\"properties\"][\"x_reglementaire\"])\n",
    "    pollution[\"y_reglementaire\"].append(data_pm10[\"features\"][i][\"properties\"][\"y_reglementaire\"])\n",
    "    pollution[\"OBJECTID\"].append(data_pm10[\"features\"][i][\"properties\"][\"OBJECTID\"])\n",
    "    \n",
    "for i in range(len(data_no2[\"features\"])):\n",
    "    \n",
    "    pollution[\"longitude\"].append(data_no2[\"features\"][i][\"geometry\"][\"coordinates\"][0])\n",
    "    pollution[\"lattitude\"].append(data_no2[\"features\"][i][\"geometry\"][\"coordinates\"][1])\n",
    "    pollution[\"nom_dept\"].append(data_no2[\"features\"][i][\"properties\"][\"nom_dept\"])\n",
    "    pollution[\"nom_com\"].append(data_no2[\"features\"][i][\"properties\"][\"nom_com\"])\n",
    "    pollution[\"insee_com\"].append(data_no2[\"features\"][i][\"properties\"][\"insee_com\"])\n",
    "    pollution[\"nom_station\"].append(data_no2[\"features\"][i][\"properties\"][\"nom_station\"])\n",
    "    pollution[\"code_station\"].append(data_no2[\"features\"][i][\"properties\"][\"code_station\"])\n",
    "    pollution[\"typologie\"].append(data_no2[\"features\"][i][\"properties\"][\"typologie\"])\n",
    "    pollution[\"influence\"].append(data_no2[\"features\"][i][\"properties\"][\"influence\"])\n",
    "    pollution[\"nom_poll\"].append(data_no2[\"features\"][i][\"properties\"][\"nom_poll\"])\n",
    "    pollution[\"id_poll_ue\"].append(data_no2[\"features\"][i][\"properties\"][\"id_poll_ue\"])\n",
    "    pollution[\"valeur\"].append(data_no2[\"features\"][i][\"properties\"][\"valeur\"])\n",
    "    pollution[\"unite\"].append(data_no2[\"features\"][i][\"properties\"][\"unite\"])\n",
    "    pollution[\"metrique\"].append(data_no2[\"features\"][i][\"properties\"][\"metrique\"])\n",
    "    pollution[\"date_debut\"].append(data_no2[\"features\"][i][\"properties\"][\"date_debut\"])\n",
    "    pollution[\"date_fin\"].append(data_no2[\"features\"][i][\"properties\"][\"date_fin\"])\n",
    "    pollution[\"x_wgs84\"].append(data_no2[\"features\"][i][\"properties\"][\"x_wgs84\"])\n",
    "    pollution[\"y_wgs84\"].append(data_no2[\"features\"][i][\"properties\"][\"y_wgs84\"])\n",
    "    pollution[\"x_reglementaire\"].append(data_no2[\"features\"][i][\"properties\"][\"x_reglementaire\"])\n",
    "    pollution[\"y_reglementaire\"].append(data_no2[\"features\"][i][\"properties\"][\"y_reglementaire\"])\n",
    "    pollution[\"OBJECTID\"].append(data_no2[\"features\"][i][\"properties\"][\"OBJECTID\"])\n",
    "    \n",
    "\n",
    "data_f = DataFrame(pollution)\n",
    "data_f.to_csv(DATA_FILE, index=None)\n",
    "\n",
    "\n",
    "data_f = DataFrame(pollution)\n",
    "print (data_f.iloc[1:3])\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
