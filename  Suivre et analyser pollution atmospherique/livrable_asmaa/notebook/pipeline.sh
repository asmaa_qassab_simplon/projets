# PIPELINE PROD DU PROJET 2 
clear
echo -e "\033[31m "ce script va effectuer les opérations suivantes:" \033[0m"
echo -e "\033[31m "1 - Down - Up des process Docker Postgres - Metabase" \033[0m"
echo -e "\033[31m "2 - Création et alimentation des Databases Projet 2" \033[0m"
echo -e "\033[31m "3 - Pipeline en mode sudo" \033[0m"
echo "Voulez-vous continuer ? [O/N]"
read mot
if [ "$mot" = "O" ]
then
echo -e "\033[32m ""Script en cours d'execution"" \033[0m"

sudo docker-compose down  # arrêt Docker
sudo docker-compose up -d # démarrage Docker
sudo docker-compose ps    # démarrage du compose pour utilisation mutli-conteneur Postgres - Metabase

echo "Création covid db"
echo "Création pollution db"
python3 create_base.py    # création des databases

echo "Création covid.csv"
echo "Création pollution.csv"
python3 covid19_json_to_csv.py  # extraction des datasets en CSV
python3 pollution_json_to_csv.py  # extraction des datasets en CSV

echo "Copie des 2 csv dans Postgres"
sudo cp covid.csv .docker/data/   # copie des CSV dans Postgres
sudo cp pollution.csv .docker/data/ # copie des CSV dans Postgres

echo "Création tables - import Data dans les 2 db"
python3 covid19_create_tables.py  # création et import dans Postgres
python3 pollution_create_tables.py # création et import dans Postgres

# sudo cp pipeline.sh /etc/cron.hourly/pipeline.sh
crontab - l

echo -e "\033[32m"Script terminé" \033[0m"

else
echo -e "\033[31m "script interrompu" \033[0m"
fi

