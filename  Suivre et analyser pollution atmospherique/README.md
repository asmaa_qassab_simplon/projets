# Projet_2-Groupe_1

Informations projet

    Technologies utilisées :
    Base de données : PostgreSQL et historisation des données
    Langages de programmation : Python
    Visualisation : Métabase / Jupyter Notebooks
    Méthode agile SCRUM
    Gitlab
    Lien du projet : Suivre et analyser pollution atmospherique

Durant ce projet d’une durée de deux semaines nous devions réaliser un tableau de bord concernant la qualité de l’air, une cartographie de la région avec les différents polluants mise à jour toutes les heures, un notebook Jupyter tutoriel et montrer l’impact du confinement sur la pollution atmosphérique dans la région. Pour ce faire, nous avons crée un script python permettant de récolter les données, une connexion avec une base de donnée Base de données : PostgreSQL et un système d’historisation de la donnée. Pour la visualisation des données, nous nous sommes servies de Métabase pour le tableau de bord et utiliser le module folium pour la cartographie. Enfin un Jupyter notebook a été réalisé pour donner un maximun d’informations sur l’utilisation de nos scripts et de nos bases de données.

