{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import psycopg2      # Librairie Connect to postgres DB\n",
    "from psycopg2.extensions import parse_dsn\n",
    "from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT\n",
    "#from psycopg2 import connect, extensions, sql\n",
    "\n",
    "# string for the new database name to be created\n",
    "sql = '''CREATE database covid19'''\n",
    "sql1 = '''CREATE database pollution'''\n",
    "\n",
    "\n",
    "db_dsn = \"postgres://postgres:test@localhost:5432/\"\n",
    "db_args = parse_dsn(db_dsn)\n",
    "#con = psycopg2.connect(\"user='postgres' password='test'\")\n",
    "conn = psycopg2.connect(**db_args)\n",
    "cur = conn.cursor()\n",
    "conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)\n",
    "cur.execute(sql)\n",
    "cur.execute(sql1)\n",
    "\n",
    "conn.commit()\n",
    "\n",
    "cur.close()\n",
    "conn.close() \n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
